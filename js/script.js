function copyText(element) {
  var color = element.trim();
  var aux = document.createElement('input');
  aux.setAttribute('value', color.toUpperCase());
  document.body.appendChild(aux);
  aux.select();
  document.execCommand('copy');
  document.body.removeChild(aux);
  $('.toast').toast('show');
}
